import express, { Router } from 'express'
import * as http from 'http'
import * as winston from 'winston'
import * as expressWinston from 'express-winston'
import cors from 'cors'
import debug from 'debug'
import dotenv from 'dotenv'
import { CommonRouter } from './common/common.router.config'
import { UserRoute } from './router/user.router'

const debugLog: debug.IDebugger = debug('app')
const app: express.Application = express()
const dontenvResult = dotenv.config({ path: '.env' })


//config router
const router: Array<CommonRouter> = []
router.push(new UserRoute(app))


if (dontenvResult.error) {
    throw dontenvResult.error
}

const server: http.Server = http.createServer(app)
const port = process.env.APP_PORT

// here we are adding middleware to parse all incoming requests as JSON 
app.use(express.json())

// here we are adding middleware to allow cross-origin requests
app.use(cors())



// here we are preparing the expressWinston logging middleware configuration,
// which will automatically log all HTTP requests handled by Express.js
const loggerOption: expressWinston.LoggerOptions = {
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
        winston.format.json(),
        winston.format.prettyPrint(),
        winston.format.colorize({all:true})
    )
}

if (!process.env.DEBUG) {
    loggerOption.meta = false ; // when not debugging, log requests as one-liners
   
}


// initialize the logger with the above configuration
app.use(expressWinston.logger(loggerOption))

const runnigMessage = `server running at http://localhost:${port}`
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(runnigMessage)
})

server.listen(port, () => {
    router.forEach((route) => {
        debugLog( `router configured for ${route.getName()}`)
    })
    // our only exception to avoiding console.log(), because we
    // always want to know when the server is done starting up
    console.log(runnigMessage)
})

export {app, server}