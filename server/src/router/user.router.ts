import express from 'express'
import { CommonRouter } from '../common/common.router.config'
import userController from '../app/controller/user.controller'
import userMiddleware from '../app/middleware/user.middleware'
export class UserRoute extends CommonRouter {

    constructor(app:express.Application) {
        super(app, 'User Router')
    }

    configureRoutes(): express.Application {

        this.app.route('/users')
            .get(userController.listUser)
        
        this.app.param(`userId`, userMiddleware.extractUserId)

        this.app.route(`/users/:userId`)
            .get(userController.getUserById)

        return this.app
    }

}