import userDao from "../DAOs/user.dao";
import { UserDto } from "../DTOs/user.dto";
import { CRUD } from "../../common/interface/crud.interface";

class UserService implements CRUD {


    async list(limit: number, page: number)  {
        return userDao.getUsers
    }

    async getUserById(id: string) {
        return userDao.getUserById(id)
    }

}

export default new UserService()