import { UserDto } from '../DTOs/user.dto';
class UserDao {
    users: Array<UserDto> = []
    
    constructor() {
        console.log('create new instance of user DAO');
        
    }


    async getUsers() {
        return this.users
    }

    async getUserById(id: string) {
        return this.users.find((user) => user.id === id)
    }


}

export default new UserDao()