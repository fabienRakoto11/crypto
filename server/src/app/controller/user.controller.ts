import express from 'express'
import userService from '../service/user.service'
import debug from 'debug'

const log: debug.IDebugger = debug('app:user-controller')

class UserController  {

    async listUser(req: express.Request, res: express.Response) {
        const users = await userService.list(100, 1)
        return res.status(200).json(users)
    }

    async getUserById(req:express.Request, res:express.Response) {
        const user = await userService.getUserById(req.body.id)
        return res.status(200).json(user)
    }

}

export default new UserController()