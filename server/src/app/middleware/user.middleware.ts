import express from 'express'
import debug from 'debug'

const log: debug.IDebugger = debug('app:user-middleware')

class UserMiddleware {

    extractUserId(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        log(`extract userId ${req.params.userId}`)
        req.body = {
            ...req.body, 
            id:req.params.userId
        }
        next()
    }

}

export default new UserMiddleware()